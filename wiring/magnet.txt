
Red Wire: C1 (3V3 Pin) -> +Rail
Black Wire: J3 (GND Pin) -> -Rail

White Wire: A10 (MOSI Pin) -> I25 (Din Pin)
Green Wire: A11 (MOS0 Pin) -> J24 (Dout Pin)
Yellow Wire: B12 (SCLK Pin) -> I23 (CLK Pin)
Orange Wire: J12 (CE0 Pin) -> J26 (CS/SHDN Pin)

MCP3008 straddles center line with the corner with the "dot" at E20

A20 (CH0 Pin) -> A36
I20 (Vdd Pin) -> +Rail
I21(Vref Pin) -> +Rail
I22 (AGND Pin) -> -Rail
I27 (DGND Pin) -> -Rail

I labeled the + pin with tape, plug it into:
B37
Plug the "out" pin to B36 
and "-" pin B35

A37 -> +Rail
A35 -> -Rail

