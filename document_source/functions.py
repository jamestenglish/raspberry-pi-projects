# the "def" keywoard means:
# define a function with this name
def print_one_two_three(): #don't forget ()'s or :
	print(1)
	print(2)
	print(3)

# It is defined but doesn't do anything until we call it:
print_one_two_three()
print_one_two_three()

# You can pass arguments in to a function to use
# the arguement becomes a variable inside your function
# that you can use
def print_if_true(condition):
	if(condition):
		print("It is true :)")
	else:
		print("It is false :(")

def print_name(name):
	print("Your name is: "+name)

# now call it:
print_if_true(1 > 2)
print_if_true(3 < 3.3 and 4 > 3.3)

print("")

print_name("James")

# Things can still go wrong
# so be very careful
print_name(1)

# The regular print() that we've been using
# To print stuff to the screen
# is just a function, but it is already
# built into python so we don't have to define it
