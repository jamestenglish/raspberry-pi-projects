class Thing(object):
	def __init__(self, name):
		self.name = name
	
class Weapon(Thing):
	def __init__(self, name, hit_probability, hit_damage):
		super(Weapon, self).__init__(name)
		self.hit_probability = hit_probability
		self.hit_damage = hit_damage
		
