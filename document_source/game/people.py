import random

class Person:
	def __init__(self, name, initial_hit_points, weapon):
		self.hit_points = initial_hit_points
		self.name = name
		self.inventory = []
		self.weapon = weapon
		
	def get_damage(self):
		dice_roll = random.random()
		
		if(self.weapon.hit_probability > dice_roll):
			return self.weapon.hit_damage
		else:
			return 0
		
		
