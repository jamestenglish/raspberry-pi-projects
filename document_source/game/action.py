from thing import Weapon, Thing
import time
import sys

actions = {}

class Action(object):
	def __init__(self, action, description, execute):
		self.action = action
		self.description = description
		self.execute = execute

def move_action(direction, room, character):
	if(direction in room.directions):
		new_room = room.directions[direction]
		if(new_room):
			print "You move "+direction
			return new_room
		print "It doesn't seem like you can go that way!"
		return room
	print direction+" doesn't seem like a place you can go."
	return room
	
def get_action(thing_name, room, character):
	for thing in room.things:
		if(thing_name == thing.name):
			room.things.remove(thing)
			character.inventory.append(thing)
			print "You pick up the "+thing_name
			return room
			
	print "That doesn't seem to be here"
	return room
	
def equip_action(thing_name, room, character):
	for thing in character.inventory:
		if(thing_name == thing.name):
			if(isinstance(thing, Weapon)):
				character.inventory.remove(thing)
				character.weapon = thing
				return room
			print "You can't equip that!"
			return room
	print "You don't have that"
	return room
	
def inv_action(unused, room, character):
	for thing in character.inventory:
		print thing.name
		
	return room
			
			
def kill_action(person_name, room, character):
	for person in room.people:
		if(person.name == person_name):
			while(character.hit_points >= 0 and person.hit_points >= 0):
				your_damage = character.get_damage()
				hit_string = "You hit "+person.name+\
				" with your "+character.weapon.name+\
				" for "+str(your_damage)

				print hit_string

				person.hit_points = person.hit_points - your_damage
				their_damage = person.get_damage()
				hit_string = person.name+" hit you with its "+\
				person.weapon.name+" for "+str(their_damage)

				print hit_string
				character.hit_points = character.hit_points - their_damage
				time.sleep(1)
			if(character.hit_points <= 0):
				print "You have died"
				sys.exit()
			print "You killed "+person.name
			return room
	print "That person doesn't seem to be here"
	return room
	
def look_action(unused, room, character):
	print "You look around and see:"
	print room.description
	print ""
	if(room.people):
		print "Currently in the room: "
		for person in room.people:
			print person.name
		print ""	
	if(room.things):
		print "Currently on the floor: "
		for thing in room.things:
			print thing.name
		print " "
	exits = "";
	for direction, value in room.directions.iteritems():
		if(value):
			exits = exits + direction + " "
	print "Exits: "+exits
	return room

def help_action(unused, room, character):
	for key,value in actions.iteritems():
		print(value.action+":")
		print("  "+value.description)
		print("")
	return room

def exit_action(unused, room, character):
	print("  Goodbye!")
	sys.exit()

	





move = Action("move", "Usage: move <direction>\n"
	"Moves you in that direction", move_action)
actions[move.action] = move

get = Action("get", "Usage: get <thing>\n"
	"Pick up the thing off the floor", get_action)
actions[get.action] = get

equip = Action("equip", "Usage: equip <thing>\n"
	"Equip the thing as your weapon. It must be a weapon.", equip_action)
actions[equip.action] = equip

inv = Action("inv", "Usage: inv\n"
	"List your inventory", inv_action)
actions[inv.action] = inv

kill = Action("kill", "Usage: kill <person>\n"
	"Attacks the person", kill_action)
actions[kill.action] = kill

look = Action("look", "Usage: look\n"
	"Looks around", look_action)
actions[look.action] = look

leave = Action("exit", "Usage: exit\n"
	"Exit the game", exit_action)
actions[leave.action] = leave

help = Action("help", "Usage: help\n"
	"Displays the help", help_action)
actions[help.action] = help
