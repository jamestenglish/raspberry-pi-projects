class Room:
	
	def __init__(self, description):
		self.description = description
		self.directions = {'north':None, 'south':None, 'east':None, 'west':None}
		self.things = []
		self.people = []
		
		
	def link(self, direction, room):
		self.directions[direction] = room
	
