from room import Room
from thing import Weapon, Thing
from people import Person
from action import actions
import time

import sys
import signal


def signal_handler(signal, frame):
    print "Exit!"
    sys.exit(0)

# Setup the rooms
# Create new rooms with descriptions
room_a = Room("Cold stone cover the walls and floor. "
	"There is very little light being cast by a flickering "
	"torch bolted to the wall")
room_b = Room("The cold stone walls seem to glisten in "
	"the pale light. A red liquid is smeared all over the floor")
room_c = Room("This room is nearly dark. Pools of blood "
	"can just barely be made out on the floor")

# Link the rooms together
room_a.link("north", room_b)
room_b.link("south", room_a)
room_b.link("east", room_c)
room_c.link("west", room_b)

# Create some items
axe = Weapon("axe", 0.8, 10)
fist = Weapon('fist', 0.5, 5)
stone = Thing("stone")

# put the items in the rooms
room_b.things.append(axe)
room_b.things.append(stone)

# create some people
you = Person("you", 100, fist)
monster = Person("beast", 100, fist)

# put the monsters in the rooms
room_c.people.append(monster)


current_room = room_a

# This is what controls the game, probably best not to touch this
while True:
	action_input = raw_input("Enter command ("+str(you.hit_points)+" hit points): ")+" "
	actions_array = action_input.split(" ", 1)
	action = actions_array[0]
	arguments = actions_array[1].strip()
	if(action in actions):
		current_room = actions[action].execute(arguments, current_room, you)
	else:
		print "I don't understand that!"
	

