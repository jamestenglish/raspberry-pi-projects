
count = 1 # set the count variable to 1

while(count <= 10): 
	print(count) # display count
	count = count + 1 # make the count variable
	                  # 1 bigger
# This code reads in english:
# While the stuff in the parathensis is true
# keep executing the indented stuff

print("Finished") # This prints after the loop