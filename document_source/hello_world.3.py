# This is a comment, anything after the "#" is completely ignored
# It is good to sometimes leave descriptions of what you are doing
 
# This program prints the text "Hello, world!" to the screen
 
print("Hello, world!") # Remember, it is everything AFTER the #
# so the printing will still happen but after it will be ignored