# You can set variables to be true or false
is_true = True
is_false = False

print(is_true)
print(is_false)
print("")

# You can also do tests
one = 1
two = 2

is_greater = one > two
# Here the is_greater variable is assigned true or false
# depending on the outcome of if the variable "one" is greater
# than the variable "two"

#Nope
print(is_greater)
print("")

is_greater = two > one

#Yup
print(is_greater)
print("")

# test if two things are equal
is_equal = 2 == two
# remember 1 equals sign assigns values to variables
# so we have to use 2 equals signs to test for equality

print(is_equal)
print("")

# you can also do < "less than"
# or greater than or equal to

is_greater_or_equal = two >= one
print(is_greater_or_equal)

