import spidev
import time
import os

DEBUG = 1

spi = spidev.SpiDev()
spi.open(0,0)

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def read_mcp(adcnum):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
	r = spi.xfer2([1,(8+adcnum)<<4,0])
	adcout = ((r[1]&3) << 8) + r[2]
	return adcout

# This converts the reading from the mcp3008 to fhrenheit
def get_temp(mcp_reading):
    milliVolts = mcp_reading * (3300.0/1024.0) 
    centigrade = (milliVolts - 500.0)/10.0
    f = (9.0/5.0)*centigrade+32
    return f


                
    

# Pin number the sensor is contected to the MCP 
mcp_pin_num = 0;

last_read = 0       # this keeps track of the last potentiometer value
tolerance = 1       # to keep from being jittery we'll only change
                    # volume when the pot has moved more than 1 'counts'

while True:
    # we'll assume that the sensor didn't move
    has_changed = False

    # read the analog pin
    mcp_reading = read_mcp(mcp_pin_num)

    # how much has it changed since the last read?
    adjusted = abs(mcp_reading - last_read)


    if ( adjusted > tolerance ):
        has_changed = True

    if ( has_changed ):

        print 'Temp = {temp}'.format(temp = get_temp(mcp_reading))

        # save the sensor reading for the next loop
        last_read = mcp_reading

    # hang out and do nothing for a half second
    time.sleep(0.5)
