import spidev
import time
import os
from psychopy.sound import SoundPygame
import sys
import signal

def signal_handler(signal, frame):
    print "Exit!"
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

DEBUG = 1

spi = spidev.SpiDev()
spi.open(0,0)

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def read_mcp(adcnum):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
	r = spi.xfer2([1,(8+adcnum)<<4,0])
	adcout = ((r[1]&3) << 8) + r[2]
	return adcout

# This converts the reading from the mcp3008 to fhrenheit
def get_hz(mcp_reading):
    hz= (float(mcp_reading) - 300.0) * (1600.0/800.0)
    if(hz < 23):
        hz = 23
    if(hz > 1600):
        hz = 1600
    print str(hz)
    return hz

                
    

# Pin number the sensor is contected to the MCP 
mcp_pin_num = 0;



while True:

    # read the analog pin
    mcp_reading = read_mcp(mcp_pin_num)
    print "reading: "+str(mcp_reading)
    sound = SoundPygame(value=get_hz(mcp_reading),secs=0.3)
    sound.play()
    time.sleep(0.1)
