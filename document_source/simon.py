#!/usr/bin/env python

import RPi.GPIO as GPIO #This allows you to control
                        #the pins

import time             #This allows you to do timing things

from random import choice #allows us to pick a random choice

import sys  #allows us to exit the program

GPIO.setmode(GPIO.BCM)

LED_1 = 7  #CE1
LED_2 = 8  #CE0
LED_3 = 25
LED_4 = 24

BUTTON_1 = 22  #SCKL
BUTTON_2 = 10  #MIS0
BUTTON_3 = 9 #MIS1
BUTTON_4 = 11 


GPIO.setup(LED_1, GPIO.OUT)  #We are going to use it for output
GPIO.setup(LED_2, GPIO.OUT)
GPIO.setup(LED_3, GPIO.OUT)
GPIO.setup(LED_4, GPIO.OUT)

GPIO.setup(BUTTON_1, GPIO.IN)
GPIO.setup(BUTTON_2, GPIO.IN)
GPIO.setup(BUTTON_3, GPIO.IN)
GPIO.setup(BUTTON_4, GPIO.IN)

CHOICES = [LED_1, LED_2, LED_3, LED_4]

#Waits until a button is pressed, then return the button pressed
def read_button_press():
    while True:
        if(GPIO.input(BUTTON_1)):
            while(GPIO.input(BUTTON_1)):
                time.sleep(0.1)  #wait until the button is released
            return BUTTON_1
        if(GPIO.input(BUTTON_2)):
            while(GPIO.input(BUTTON_2)):
                time.sleep(0.1)
            return BUTTON_2
        if(GPIO.input(BUTTON_3)):
            while(GPIO.input(BUTTON_3)):
                time.sleep(0.1)
            return BUTTON_3
        if(GPIO.input(BUTTON_4)):
            while(GPIO.input(BUTTON_4)):
                time.sleep(0.1)
            return BUTTON_4

#Get the LED for the button
def get_led_for_button(button_pressed):
    if(button_pressed == BUTTON_1):
        return LED_1
    if(button_pressed == BUTTON_2):
        return LED_2
    if(button_pressed == BUTTON_3):
        return LED_3
    if(button_pressed == BUTTON_4):
        return LED_4

sequence = []  #The sequence the player has to match
           #It starts off empty

while True:
    
    sequence.append(choice(CHOICES))  #Add a light to the sequence

    for led in sequence:  #Play the LEDs
        print "Playing LED: "+str(led)
        GPIO.output(led, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(led, GPIO.LOW)
        time.sleep(1)

    for led in sequence: #Now read the input
        button = read_button_press()
        print "Read button: "+str(button)
        led_for_button = get_led_for_button(button)
        if(led != led_for_button):  #The player screwed up!!!
            #blink all the LEDs 3 times to show the user they failed
            for i in range(1,3):  
                GPIO.output(LED_1, GPIO.HIGH)
                GPIO.output(LED_2, GPIO.HIGH)
                GPIO.output(LED_3, GPIO.HIGH)
                GPIO.output(LED_4, GPIO.HIGH)
                time.sleep(1)
                GPIO.output(LED_1, GPIO.LOW)
                GPIO.output(LED_2, GPIO.LOW)
                GPIO.output(LED_3, GPIO.LOW)
                GPIO.output(LED_4, GPIO.LOW)
                time.sleep(1)
            sys.exit()  #exit the program!
                
    


