#!/usr/bin/env python

import time
from letters import LETTERS

from Adafruit_Raspberry_Pi_Python_Code\
.Adafruit_LEDBackpack.Adafruit_8x8 import EightByEight

#Setup the LED grid
grid = EightByEight(address=0x70)

#This reads text from the input
input_text = raw_input("Enter some text: ")

#convert the text to all lower case
lower_case = input_text.lower()

#for each letter in the text
for letter in lower_case:
    
    #find the letter array, tak a look at letters.py
    letter_arr = LETTERS[letter] 
    for offset in reversed(range(-8,8)):
        for y in range(0,8):
            for x in range(0,8):
                
                if(offset + x > 7 or offset + x < 0):
                    continue
                    
                if(letter_arr[y][x]):
                    grid.setPixel(offset+x, y)
        time.sleep(0.1)
        grid.clear()
                
