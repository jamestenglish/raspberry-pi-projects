import spidev
import time
import os

DEBUG = 1

spi = spidev.SpiDev()
spi.open(0,0)

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def read_mcp(adcnum):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
	r = spi.xfer2([1,(8+adcnum)<<4,0])
	adcout = ((r[1]&3) << 8) + r[2]
	return adcout



                
    

# Pin number the sensor is contected to the MCP 
mcp_pin_num = 0;

last_read = 0       # this keeps track of the last potentiometer value
                    # volume when the pot has moved more than 1 'counts'

has_magnet = True
while True:
    # read the analog pin
    mcp_reading = read_mcp(mcp_pin_num)

    if(mcp_reading > 5):
        if(has_magnet):
            has_magnet = False
            print "No Magnet"
    else:
        if(not has_magnet):
            has_magnet = True
            print "Magnet"
    # hang out and do nothing for a half second
    time.sleep(0.5)
