one = 1 # this variable stores the number 1
two = 2 # this variable stores the number 2
 
three = one + two
# This line says: get the value from the variable 
# named "one" and add it to the value from the variable 
# named "two" and put the result into a variable named "three"
 
four = three + 1
# you can also mix and match variables and values
 
five = 6
# Becareful how you name variables! You can name them whatever 
# you want but it is usually good to give them
# Descriptive and correct names.
# This line stores the number 6 in a variable named "five", very confusing...