#!/usr/bin/env python

import time

from Adafruit_Raspberry_Pi_Python_Code.\
Adafruit_LEDBackpack.Adafruit_8x8 import EightByEight

#Setup the LED grid
grid = EightByEight(address=0x70)

x = 0 # any number between 0-7
y = 0 # any number between 0-7

#set a grid pixel:
grid.setPixel(x,y)
grid.clear()

