# For loops are great for looping over lists
foods = ["apples", "peaches", "pumpkin pie"]

for food in foods:
	print("Food is: "+food)

print("Done printing foods") # This is not indented so it doesn't
                             # belong to the loop

# This reads: For each thing in foods, assign it to a variable
# called "food" and then go on to the next item
# When it has reached the end of the list it stops

# We can re-write the while program like this
for count in range(1,11):
	print(count)

# Range creates a list of numbers
# from the first number to the last number - 1