import time

from Adafruit_Raspberry_Pi_Python_Code.\
Adafruit_LEDBackpack.Adafruit_8x8 import EightByEight

#Setup the LED grid
grid = EightByEight(address=0x70)

for x in range(0,8): #x will be 0,1,2,3,4,5,6,7
	for y in range(0,8): #y will be 0,1,2,3,4,5,6,7
		grid.setPixel(x, y)
		time.sleep(0.1)
		grid.clear()
