import random


random_number = random.random()
# this gives a number between 0 and 1
# Lets convert it to an int between 1-100
number_to_guess = int(random_number*100)+1

is_number_guessed = False
while(not is_number_guessed): # "not" says take the opposite
	input_text = raw_input("Enter a number and press enter: ")
	if(input_text.isdigit): # test to see if what they entered is a number
		number_guessed = int(input_text)
		if(number_guessed > number_to_guess):
			print("Too high.")
		if(number_guessed < number_to_guess):
			print("Too low.")
		if(number_guessed == number_to_guess):
			is_number_guessed = True
	else:
		print("That isn't a number!")

print("You win!")