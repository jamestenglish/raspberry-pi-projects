one = 1
two = 2

# test if one > two, if that is true then run the code indented under the if
if(one > two): # don't forget the colon ":""
	print("Test 1:") # this won't display
	print("Variable one is greater than two") # neither will this

# In english this code reads: If the part in the parathensis is true,
# then execute the indented code

one = 3 # this part of the code is not indented under the if 
# so it doesn't belong to that block and will be executed always

# try again now that we changed the value of one
if(one > two): # don't forget the colon ":""
	print("Test 2:")
	print("Variable one is greater than two")

# you can have multiple tests
one = 1
two = 2

test_value = 2.2

# This will check if one > test value (which is false)
# Or if test_value > two (which is true)
# if any part of the "or" is true the whole thing is true
if(one > test_value or test_value > two):
	print("Test 3 passed")

# This will check if one > test value (which is false)
# and if test_value > two (which is true)
# if any part of the "and" is false the whole thing is false
if(one > test_value and test_value > two):
	print("Test 4 passed") # this doesn't display

# you can also see add an else
if(one > test_value and test_value > two):
	print("Test 4 passed") # this doesn't display
else: 
	print("Test 4 failed")

# The code indented after the else gets excuted if the if check fails

# In english this reads: If the part in the parathensis is true,
# Then do the indented code
# Else (it wasn't true)
# Then do the other indented code
