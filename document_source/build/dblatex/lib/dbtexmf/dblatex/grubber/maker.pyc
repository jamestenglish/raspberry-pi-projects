³ò
ÖPc           @   s   d  Z  d d k Z d d k Z d d k Z d d k l Z l Z d e f d     YZ d e f d     YZ d e f d	     YZ	 d
 d d     YZ
 d S(   sP   
This module contains all the classes used to manage the building
dependencies.
iÿÿÿÿN(   t   _t   msgt   Dependc           B   st   e  Z d  Z d h  h  d  Z d   Z d   Z d d  Z d   Z d   Z	 d   Z
 d	   Z d
   Z d   Z RS(   sG  
    This is a base class to represent file dependencies. It provides the base
    functionality of date checking and recursive making, supposing the
    existence of a method `run()' in the object. This method is supposed to
    rebuild the files of this node, returning zero on success and something
    else on failure.
    c         C   sX   | |  _  | o | |  _ n
 g  |  _ |  i   | |  _ d |  _ d |  _ | |  _ d S(   s{  
        Initialize the object for a given set of output files and a given set
        of sources. The argument `prods' is a list of file names, and the
        argument `sources' is a dictionary that associates file names with
        dependency nodes. The optional argument `loc' is a dictionary that
        describes where in the sources this dependency was created.
        i    N(   t   envt   prodst   set_datet   sourcest   makingt   Nonet
   failed_dept   loc(   t   selfR   R   R   R
   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyt   __init__   s    		
			c         C   se   |  i  g  j o d |  _ nE y% t t t i i |  i    |  _ Wn t j
 o d |  _ n Xd S(   sÖ   
        Define the date of the last build of this node as that of the most
        recent file among the products. If some product does not exist or
        there are ne products, the date is set to None.
        N(	   R   R   t   datet   maxt   mapt   ost   patht   getmtimet   OSError(   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR   (   s    %c         C   sK   |  i  p d Sn x2 |  i i   D]! } | i  |  i  j o d Sq" q" Wd S(   s¥   
        Check the dependencies. Return true if this node has to be recompiled,
        i.e. if some dependency is modified. Nothing recursive is done here.
        i   i    (   R   R   t   values(   R   t   src(    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyt   should_make<   s    
 i    c         C   s!  |  i  o d GHd Sn d |  _  d |  _ | } xd |  i i   D]S } | i   } | d j o d |  _  | i |  _ d Sn | d j o
 d } q? q? W| p |  i   oj | o |  i   } n |  i   } | o d |  _  |  |  _ d Sn t	 t
 i
    |  _ d |  _  d Sn d |  _  d S(   sà  
        Make the destination file. This recursively makes all dependencies,
        then compiles the target if dependencies were modified. The semantics
        of the return value is the following:
        - 0 means that the process failed somewhere (in this node or in one of
          its dependencies)
        - 1 means that nothing had to be done
        - 2 means that something was recompiled (therefore nodes that depend
          on this one have to be remade)
        s   FIXME: cyclic makei   i    i   N(   R   R   R	   R   R   t   makeR   t	   force_runt   runt   intt   timeR   (   R   t   forcet	   must_makeR   t   ret(    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR   H   s8    
		 						c         C   s
   |  i    S(   s   
        This method is called instead of 'run' when rebuilding this node was
        forced. By default it is equivalent to 'run'.
        (   R   (   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR   }   s    c         C   s   |  i  S(   s   
        Return a reference to the node that caused the failure of the last
        call to "make". If there was no failure, return None.
        (   R	   (   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyt   failed   s    c         c   s   d o	 d Vn d S(   sT   
        Report the errors that caused the failure of the last call to run.
        N(   R   (   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyt
   get_errors   s    c         C   s   xL |  i  D]A } t i i |  o( t i t d  |  t i |  q
 q
 Wx! |  i i	   D] } | i
   q_ Wd |  _ d S(   sh   
        Remove the files produced by this rule and recursively clean all
        dependencies.
        s   removing %sN(   R   R   R   t   existsR   t   logR    t   unlinkR   R   t   cleanR   R   (   R   t   fileR   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR$      s    
  c         C   s   d S(   sB   
        Reinitializing depends on actual dependency leaf
        N(    (   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyt   reinit   s    c         C   sR   |  i  h  j o |  i Sn g  } x* |  i  i   D] } | i | i    q1 W| S(   s   
        Return a list of all source files that are required by this node and
        cannot be built, i.e. the leaves of the dependency tree.
        (   R   R   R   t   extendt   leaves(   R   R   t   dep(    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR(   ¥   s     N(   t   __name__t
   __module__t   __doc__R   R   R   R   R   R   R   R    R$   R&   R(   (    (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR      s   		5					t
   DependLeafc           B   s)   e  Z d  Z d   Z d   Z d   Z RS(   sc   
    This class specializes Depend for leaf nodes, i.e. source files with no
    dependencies.
    c         O   s#   t  i |  | d t |  | d S(   s   
        Initialize the node. The arguments of this method are the file
        names, since one single node may contain several files.
        R   N(   R   R   t   list(   R   R   t   destt   args(    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR   ·   s    c         C   sb   t  |  i  d j o( t i t d  |  i d |  i  n! t i t d  |  i |  i  d S(   Ni   s   %r does not existi    s   one of %r does not exist(   t   lenR   R   t   errorR    R
   (   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR   ¾   s    ( c         C   s   d  S(   N(    (   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR$   Æ   s    (   R*   R+   R,   R   R   R$   (    (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR-   ²   s   		t   DependShellc           B   s    e  Z d  Z d   Z d   Z RS(   sR   
    This class specializes Depend for generating files using shell commands.
    c         K   s    t  i |  | |  | |  _ d  S(   N(   R   R   t   cmd(   R   R   R4   R0   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR   Î   s    c         C   sp   t  i t d  |  i d  t i |  i d t  i } | d j o& t  i t d  |  i d  d Sn d S(   Ns
   running %si    t   stdouts   execution of %s failedi   (   R   t   progressR    R4   t
   subprocesst   callR5   R2   (   R   t   rc(    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR   Ò   s    (   R*   R+   R,   R   R   (    (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR3   Ê   s   	t   Makerc           B   s>   e  Z d  Z d   Z d   Z d   Z d d  Z d   Z RS(   sa   
    Very simple builder environment. Much simpler than the original rubber
    Environment.
    c         C   s   g  |  _  d  S(   N(   t	   dep_nodes(   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR   à   s    c         C   s!   |  i  p d  Sn |  i  d Sd  S(   Niÿÿÿÿ(   R;   R   (   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyt   dep_lastã   s    
c         C   s   |  i  i |  d  S(   N(   R;   t   append(   R   R)   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyt
   dep_appendé   s    i    c         C   sH   |  i  p d Sn |  i  d i d |  } | d j o d Sn d Sd  S(   Ni    iÿÿÿÿR   (   R;   R   (   R   R   R9   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR   ì   s    
c         C   s   |  i    d  S(   N(   R   (   R   (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR&   ö   s    (   R*   R+   R,   R   R<   R>   R   R&   (    (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pyR:   Û   s   			
(    (   R,   R   R   R7   R   R    t   objectR   R-   R3   R:   (    (    (    s$   lib/dbtexmf/dblatex/grubber/maker.pys   <module>   s   ¥