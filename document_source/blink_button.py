#!/usr/bin/env python

import RPi.GPIO as GPIO #This allows you to control
                        #the pins

import time             #This allows you to do timing things


GPIO.setmode(GPIO.BCM)

LED_PIN = 23       #The pin we want to control

BUTTON_PIN = 22  #The pin to listen for the button

GPIO.setup(LED_PIN, GPIO.OUT)  #We are going to use it for output
GPIO.setup(BUTTON_PIN, GPIO.IN)

while True:
    is_pressed = GPIO.input(BUTTON_PIN)
    
    if(is_pressed):
        GPIO.output(LED_PIN, GPIO.HIGH)  #Turn the pin on
        time.sleep(1)  #Wait 1 second
        GPIO.output(LED_PIN, GPIO.LOW)  #Turn the pin off
        time.sleep(1)  #Wait 1 second


