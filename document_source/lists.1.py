# A list of strings that are fruit names
fruit = ["apples", "peaches", "pears"]
# the [ ] brackets mean this is a list!
# and seperate the different entries with a comma

print(fruit)

# You can also store numbers
fibonacci = [1, 1, 2, 3, 5, 8]
print(fibonacci)

# Or variables or a mix
five = 5
mix = [1, 1, 2, 3, five, 8]
print(mix)

# You can access things based on their position
# REMEMBER the position ALWAYS starts at 0
peaches = fruit[1] 
print(peaches)

# You can add or "append" things to a list
fruit.append("turnips")
print(fruit)

# Or delete things
del fruit[3]
print(fruit)

