string_variable = "Computers store text as strings"
string_variable2 = "the quotes are only there to let the"
string_variable3 = "computer know this is all one thing"
 
# computers can also store numbers
# But there are different kinds of numbers
#there are whole numbers called "integers" or "ints" for short
integer_variable = 7
 

 # Numbers with decimals are different they are called "floating point" numbers 
 # or "floats" for short
float_variable = 7.5
 
