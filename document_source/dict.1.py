# Lets create a dictionary like real life:
dictionary = {"buttocks": "The rump of an animal.",
"elbow":"The joint between the forearm and the upper arm",
"foot":"The lower extremity of the leg below the ankle"}
# Curly brackets { } mean it is a dictionary
# every entry is defined as key : value

print(dictionary)
print("") # just a blank line

# You get values from the dictionary by referencing
# the key

butt_definition = dictionary["buttocks"]
print(butt_definition)
print("") # just a blank line

# You can change values
dictionary["buttocks"] = "Hehe you said butt!"
# Remember this DOES NOT change the butt_definition variable
print(butt_definition)
print(dictionary["buttocks"])
print("") # just a blank line

#You can add things to the dictionary
dictionary["toe"] = "Any of the five digits at the end of the human foot"
print(dictionary)
print("") # just a blank line

# and remove them
del dictionary["buttocks"]
print(dictionary)
print("") # just a blank line

# If you try looking up things that aren't there you get an error
print(dictionary["buttocks"])
