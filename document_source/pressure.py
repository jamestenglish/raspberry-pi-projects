import spidev
import time
import os
import sys
import signal
from Adafruit_Raspberry_Pi_Python_Code.\
Adafruit_LEDBackpack.Adafruit_8x8 import EightByEight

#Setup the LED grid
grid = EightByEight(address=0x70)


def signal_handler(signal, frame):
    print "Exit!"
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

DEBUG = 1

spi = spidev.SpiDev()
spi.open(0,0)

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def read_mcp(adcnum):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
	r = spi.xfer2([1,(8+adcnum)<<4,0])
	adcout = ((r[1]&3) << 8) + r[2]
	return adcout

# This converts the reading from the mcp3008 to
# a number between 1 - 64
def get_in_64(mcp_reading):
    transformed_number = int(float(mcp_reading) * 64.0/1023.0) + 1
    return transformed_number

                
    

# Pin number the sensor is contected to the MCP 
mcp_pin_num = 0;

while True:

    # read the analog pin
    mcp_reading = read_mcp(mcp_pin_num)
    print "reading: "+str(mcp_reading)
    conversion = get_in_64(mcp_reading)
    print "conversion: "+str(conversion)

    count = 0
    grid.clear()
    for x in range(0,8): 
        for y in range(0,8): 
            count = count + 1
            if(count <= conversion):
                grid.setPixel(x,y)

    time.sleep(0.1)
